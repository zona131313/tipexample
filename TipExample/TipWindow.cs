﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TipExample.Tip;

namespace TipExample
{
    public partial class TipWindow : Form
    {
        private Tips _tip;

        public TipWindow()
        {
            _tip = new Tips();
            InitializeComponent();
            TipText.Text = _tip.Next();
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            TipText.Text = _tip.Previous();
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            TipText.Text = _tip.Next();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
