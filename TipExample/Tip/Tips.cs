﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TipExample.Tip
{
    public class Tips
    {
        private DataBase _db;
        private List<String> _buffer;
        private bool _isConnect;
        private int _pt;

        public Tips()
        {
            _db = DataBase.Instance;
            _buffer = new List<string>();
            _pt = -1;
        }

        ~Tips()
        {
            _db.Close();
        }

        public string Next()
        {
            if (!_isConnect)
            {
                _isConnect = _db.Open();
                if (!_isConnect)
                {
                    return "No connection";
                }
            }

            if (_pt == _buffer.Count - 1)
            {
                string command = "SELECT tiptext FROM tips ORDER BY RAND() LIMIT 1";
                var cmd = new MySqlCommand(command, _db.Connection);
                _buffer.Add(cmd.ExecuteScalar().ToString());
                _pt++;
                return _buffer.Last();
            }
            else
            {
                return _buffer.ElementAt(++_pt);
            }
        }

        public string Previous()
        {
            if (!_isConnect)
            {
                return "No connection";
            }

            if (_pt > 0)
            {
                return _buffer.ElementAt(--_pt);
            }
            else
            {
                return Current();
            }
        }

        public string Current()
        {
            if (!_isConnect)
            {
                return "No connection";
            }

            return _buffer.ElementAt(_pt);
        }
    }
}
