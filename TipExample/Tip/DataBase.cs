﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace TipExample.Tip
{
    public class DataBase
    {
        private static DataBase _instance = null;
        private MySqlConnection _connection = null;

        private const string Database = "tip";
        private const string Server = "localhost";
        private const string User = "root";
        private const string Password = "qwerty";

        private DataBase()
        {
        }

        public bool Open()
        {
            if (_connection == null)
            {
                _connection = new MySqlConnection(string.Format("Server={0}; database={1}; UID={2}; password={3}", Server, Database, User, Password));
            }

            try
            {
                _connection.Open();
            }
            catch(MySqlException e)
            {
                Console.Write(e);
                return false;
            }

            return true;
        }

        public MySqlConnection Connection
        {
            get
            {
                return _connection;
            }
        }

        public bool Close()
        {
            try
            {
                _connection.Open();
            }
            catch (MySqlException e)
            {
                Console.Write(e);
                return false;
            }

            return true;
        }

        public static DataBase Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DataBase();
                }
                return _instance;
            }
        }
    }
}
